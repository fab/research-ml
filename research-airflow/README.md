# research-airflow

Orchestration of machine learning workloads for research as airflow directed acyclic graphs in the `dags` directory.

Tooling for specific fpr running orchestrated machine learning pipelines in the data engineering infrastructure will eventually be in a central repository. 

