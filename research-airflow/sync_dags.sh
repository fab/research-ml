
# TODO
# this should be done by CI

# install research-ml from github
# ssh an-airflow1002.eqiad.wmnet "/usr/lib/airflow/bin/pip uninstall -y research-transform"
# install_repo="https_proxy=http://webproxy.eqiad.wmnet:8080 /usr/lib/airflow/bin/pip install -e 'git+https://gitlab.wikimedia.org/fab/research-ml.git@fab/knowledge_gaps#egg=research_transform&subdirectory=research-transform'"
# ssh an-airflow1002.eqiad.wmnet "$install_repo"

# clone and install research-ml from github
# clone_repo="https_proxy=http://webproxy.eqiad.wmnet:8080 git clone https://gitlab.wikimedia.org/fab/research-ml.git"
# ssh an-airflow1002.eqiad.wmnet "$clone_repo"
# install_repo="cd research-ml/research-transform/ && https_proxy=http://webproxy.eqiad.wmnet:8080 git pull && /usr/lib/airflow/bin/pip install ."
# ssh an-airflow1002.eqiad.wmnet "$install_repo"

# rsync research-transform working branch and install
# rsync -a research-transform/ an-airflow1002.eqiad.wmnet:research-transform
# install_repo="cd research-transform/ && /usr/lib/airflow/bin/pip install ."
# ssh an-airflow1002.eqiad.wmnet "$install_repo"

rsync -a research-airflow/dags/ an-airflow1002.eqiad.wmnet:dags
rsync -a research-airflow/job/ an-airflow1002.eqiad.wmnet:job

chmod_hack="sudo -u analytics-research chmod 777 /srv/airflow-research/dags"
ssh an-airflow1002.eqiad.wmnet "$chmod_hack"
mv_command="mv dags/*.py /srv/airflow-research/dags"
ssh an-airflow1002.eqiad.wmnet "$mv_command"

chmod_hack="sudo -u analytics-research chmod 777 /srv/airflow-research/job"
ssh an-airflow1002.eqiad.wmnet "$chmod_hack"
mv_command="mv job/*.py /srv/airflow-research/job"
ssh an-airflow1002.eqiad.wmnet "$mv_command"
