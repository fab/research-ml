# hackery
# custom conda envs not supported yet for airflow-research user
import os
import sys
sys.path.append('/home/fab/.local/lib/python3.7/site-packages')
# the airflow-research user does not have a home directory
os.environ['SKEIN_CONFIG']="/srv/airflow-research/.skein"
import skein

from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash import BashOperator
from airflow.utils.dates import days_ago
import time

#  ballista-executor and ballista-scheduler binary in /user/fab/rust/ on hdfs

# nyc taxi example csv also stored in for testing
hdfs_data = "/user/fab/rust/yellow_tripdata_2020-01.csv"

#TODO distributed files not implemented for ballista, what are our options to use hdfs data?
# ballista binaries should be passed as skein.File
setup_cmd = f"""
    hdfs dfs -get /user/fab/rust/ballista/ballista-* . && \
    export WORK_DIR=`pwd` && \
    chmod +x ballista-*"""

def deploy_ballista(n_executors, executor_cores=4, scheduler_cores=2, executor_memory='4 GiB', scheduler_memory='4 GiB'):

    scheduler_cmd = f"""
        {setup_cmd} && \
        RUST_LOG=info ./ballista-scheduler
        """
    executor_cmd = f"""
        {setup_cmd} && \
        echo "running executor connected to $SCHEDULER_HOST" && \
        RUST_LOG=info ./ballista-executor --work-dir $WORK_DIR --scheduler-host $SCHEDULER_HOST --external-host `hostname`  -c {executor_cores}
        """

    services = {
        "scheduler": skein.Service(
            script=scheduler_cmd,
            instances=1,
            resources=skein.Resources(memory=scheduler_memory, vcores=scheduler_cores)
        ),
        "executor": skein.Service(
            script=executor_cmd,
            instances=0,
            resources=skein.Resources(memory=executor_memory, vcores=executor_cores)
        )
    }


    spec = skein.ApplicationSpec(
        name=f"ballista",
        services=services
    )

    with skein.Client(log_level='debug') as client:

        app_client = client.submit_and_connect(spec)

        scheduler_host=""
        while scheduler_host=="":
            print("waiting for scheduler host")
            scheduler = app_client.get_containers(services=["scheduler"])[0]
            scheduler_host=scheduler.yarn_node_http_address
            print(scheduler_host)
            time.sleep(1)

        for i in range(n_executors):
            app_client.add_container(service="executor", env={'SCHEDULER_HOST': scheduler_host.split(":")[0]})

        while len(app_client.get_containers()) > 0:
            print("still running")
            time.sleep(60)
    return "done"

# RUST_LOG=info ballista-scheduler
# RUST_LOG=info ballista-executor -c 4
with DAG(
    'ballista',
    description='rust',
    schedule_interval=None,
    start_date=days_ago(2)
) as ballista:

    PythonOperator(task_id='ballista', python_callable=deploy_ballista, op_kwargs={'n_executors': 3})


def deploy_executor(scheduler_host, n_executors, n_cores):

    executor_cmd = f"""
        {setup_cmd} && \
        RUST_LOG=info ./ballista-executor --scheduler-host {scheduler_host} --external-host `hostname`  -c {n_cores}
        """

    services = {}
    for i in range(n_executors):
        services[f"executor_{i}"] = skein.Service(
            script=executor_cmd,
            resources=skein.Resources(memory='4 GiB', vcores=n_cores)
        )

    spec = skein.ApplicationSpec(
        name=f"ballista",
        services=services
    )

    with skein.Client(log_level='debug') as client:

        app_client = client.submit_and_connect(spec)
        while len(app_client.get_containers()) > 0:
            print("still running")
            time.sleep(60)


    return "done"


with DAG(
    'ballista_executors',
    description='rust',
    schedule_interval=None,
    start_date=days_ago(2)
) as ballista_executors:
    args = {
        'scheduler_host': 'analytics1071',
        'n_executors': 3,
        'n_cores': 2
        }
    PythonOperator(task_id='ballista_executors', python_callable=deploy_executor, op_kwargs=args)