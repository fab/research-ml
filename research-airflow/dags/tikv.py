# tikv key / value store via same approach
# https://tikv.org/docs/3.0/tasks/deploy/binary/#deploy-the-tikv-cluster-on-multiple-nodes-for-testing

# hackery
# custom conda envs not supported yet for airflow-research user
import os
import subprocess
import sys
sys.path.append('/home/fab/.local/lib/python3.7/site-packages')
# the airflow-research user does not have a home directory
os.environ['SKEIN_CONFIG']="/srv/airflow-research/.skein"
import skein

from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash import BashOperator
from airflow.utils.dates import days_ago


from research_transform.skein.tikv import deploy_tikv

with DAG(
    'tikv',
    description='rust',
    schedule_interval=None,
    start_date=days_ago(2)
) as tikv:

    PythonOperator(task_id='tikv', python_callable=deploy_tikv, op_kwargs={'n_pd': 3, 'n_tikv': 20})
