# hackery
# custom conda envs not supported yet for airflow-research user
import os
import sys
sys.path.append('/home/fab/.local/lib/python3.7/site-packages')

# the airflow-research user does not have a home directory
# TODO requires puppet change?
os.environ['SKEIN_CONFIG']="/srv/airflow-research/.skein"
import skein

from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash import BashOperator
from airflow.utils.dates import days_ago
import time

def ping_hdfs(name, n_hosts):

    ping_command = f"touch here && \
                    hdfs dfs -put here hosts/`hostname`_{name} && \
                    echo 'pinged hdfs' && \
                    sleep 30"

    services = {}
    for i in range(n_hosts):
        services[f'host_{i}'] = skein.Service(
            script=ping_command,
            resources=skein.Resources(memory='1 GiB', vcores=1)
        )

    spec = skein.ApplicationSpec(
        name=f"pinging_hdfs_for_{name}",
        services=services
    )

    with skein.Client(log_level='debug') as client:
        app_client = client.submit_and_connect(spec)

        try:
            while len(app_client.get_containers()) > 0:
                print("containers still running")
                time.sleep(5)
        except:
            pass

    return "done"

with DAG(
    'pinging_hdfs',
    description='skein example',
    schedule_interval=None,
    start_date=datetime(2021, 1, 1),
    catchup=False
) as dag:

    clear_pings = BashOperator(task_id="clear_pings", bash_command='hdfs dfs -rm hosts/*')
    first = PythonOperator(task_id='first_ping', python_callable=ping_hdfs, op_kwargs={'name': 'first', 'n_hosts':10 })
    second = PythonOperator(task_id='second_ping', python_callable=ping_hdfs, op_kwargs={'name': 'second', 'n_hosts':5 })
    third = PythonOperator(task_id='third_ping', python_callable=ping_hdfs, op_kwargs={'name': 'third', 'n_hosts':15 })
    list_pings = BashOperator(task_id="list_pings", bash_command='hdfs dfs -ls hosts/*')

    clear_pings >> first
    first >> second >> list_pings
    first >> third >> list_pings