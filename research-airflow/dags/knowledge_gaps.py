from airflow.models import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.providers.apache.spark.operators.spark_submit import SparkSubmitOperator
from airflow.utils.dates import days_ago

import os
import sys
os.environ['SPARK_HOME']="/usr/lib/spark2"
os.environ['JAVA_HOME']="/usr/lib/jvm/java-8-openjdk-amd64/jre"

sys.path.append('/home/fab/.local/lib/python3.7/site-packages')
os.environ['https_proxy']="http://webproxy.eqiad.wmnet:8080"


spark_config = {
    'spark.driver.extraJavaOptions' : ' '.join('-D{}={}'.format(k, v) for k, v in {
        'http.proxyHost': 'webproxy.eqiad.wmnet',
        'http.proxyPort': '8080',
        'https.proxyHost': 'webproxy.eqiad.wmnet',
        'https.proxyPort': '8080',
    }.items()),
    'spark.sql.shuffle.partitions': 512,
    'spark.dynamicAllocation.maxExecutors': 10,
    'spark.executor.memory': '8g',
    'spark.executor.cores': '4',
    'spark.driver.memory': '4g',
}

with DAG(
    dag_id='knowledge_gaps_single',
    schedule_interval=None,
    start_date=days_ago(2),
    tags=['knowledge_gap'],
) as dag1:
    
    submit_job = SparkSubmitOperator(            
        spark_binary='spark2-submit',
        application="/srv/airflow-research/job/knowledge_gaps.py", 
        task_id="knowledge_gap_metrics",
        env_vars= {
            "JAVA_HOME":"/usr/lib/jvm/java-8-openjdk-amd64/jre"
        },
        # todo do we need this after all?
        files="/etc/hive/conf/hive-site.xml",
        # dag=dag1,
        conf=spark_config,        
        # todo do we need this after all?
        principal="analytics-research/an-airflow1002.eqiad.wmnet@WIKIMEDIA",
        keytab="/etc/security/keytabs/analytics-research/analytics-research.keytab",
    )



# pseudo dags
with DAG(
    dag_id='knowledge_gaps_parallel',
    schedule_interval=None,
    start_date=days_ago(2),
    tags=['knowledge_gap'],
) as dag2:
    
    gaps = ['gender', 'sexual_orientation', 'geography', 'ccc', 'time']

    for gap in gaps:
        job = SparkSubmitOperator(            
            spark_binary='spark2-submit',
            application="/srv/airflow-research/job/knowledge_gaps.py", 
            task_id=f'{gap}_gap',
            env_vars= {
                "JAVA_HOME":"/usr/lib/jvm/java-8-openjdk-amd64/jre"
            },
            # todo do we need this after all?
            files="/etc/hive/conf/hive-site.xml",
            # dag=dag2,
            conf=spark_config,        
            # todo do we need this after all?
            principal="analytics-research/an-airflow1002.eqiad.wmnet@WIKIMEDIA",
            keytab="/etc/security/keytabs/analytics-research/analytics-research.keytab",
        )

with DAG(
    dag_id='knowledge_gaps_common',
    schedule_interval=None,
    start_date=days_ago(2),
    tags=['knowledge_gap'],
) as dag3:
    
    gaps = ['gender', 'sexual_orientation', 'geography', 'ccc', 'time']
    common = DummyOperator(task_id=f'common_steps')
    for gap in gaps:
        common >>  DummyOperator(task_id=f'{gap}_gap')



with DAG(
    dag_id='knowledge_gaps_stub',
    schedule_interval=None,
    start_date=days_ago(2),
    tags=['knowledge_gap'],
) as dag4:
    
    download_data = DummyOperator(task_id=f'download_external_data')
    common = DummyOperator(task_id=f'common_steps')
    gaps = [DummyOperator(task_id=f'{gap}_gap') for gap in ['gender', 'sexual_orientation', 'geography', 'ccc', 'time']]    
    validation = DummyOperator(task_id=f'validate_metrics')
    store_metrics = DummyOperator(task_id=f'store_metrics_in_database')

    download_data >> common
    for gap in gaps:
        common >> gap >> validation
    validation >> store_metrics
        
