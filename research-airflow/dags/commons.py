from airflow.models import DAG
from airflow.providers.apache.spark.operators.spark_submit import SparkSubmitOperator
from airflow.utils.dates import days_ago
import json


# all this should be done by the wmf operators in planning
import os
import sys
os.environ['SPARK_HOME']="/usr/lib/spark2"
os.environ['JAVA_HOME']="/usr/lib/jvm/java-8-openjdk-amd64/jre"

sys.path.append('/home/fab/.local/lib/python3.7/site-packages')
os.environ['https_proxy']="http://webproxy.eqiad.wmnet:8080"

spark_config = {
    'spark.driver.extraJavaOptions' : ' '.join('-D{}={}'.format(k, v) for k, v in {
        'http.proxyHost': 'webproxy.eqiad.wmnet',
        'http.proxyPort': '8080',
        'https.proxyHost': 'webproxy.eqiad.wmnet',
        'https.proxyPort': '8080',
    }.items()),
    'spark.jars.packages': 'org.apache.spark:spark-avro_2.11:2.4.4',
    'spark.sql.shuffle.partitions': 512,
    'spark.dynamicAllocation.maxExecutors': 10,
    'spark.executor.memory': '8g',
    'spark.executor.cores': '4',
    'spark.driver.memory': '4g',
}

with DAG(
    dag_id='download_media_from_swift',
    schedule_interval=None,
    start_date=days_ago(2),
    tags=['commons','swift', 'images'],
) as download_media_from_swift:

    job_fn_name="research_transform.swift.commons.download_commons_media"
    fn_args = json.dumps({
        "snapshot": '2021-08',
        "media_output_dir": "test_fa",
        "wiki_dbs": ["fa"]
    })

    application_args = [
        '--job-fn', job_fn_name,
        '--job-args', fn_args,
        '--spark-config', json.dumps(spark_config),
        ]

    submit_job = SparkSubmitOperator(
        spark_binary='spark2-submit',
        application="/srv/airflow-research/job/job.py",
        application_args=application_args,
        task_id=job_fn_name,
        env_vars= {
            "JAVA_HOME":"/usr/lib/jvm/java-8-openjdk-amd64/jre",
            "SPARK_HOME":"/usr/lib/spark2"
        },
        # todo do we need this after all?
        files="/etc/hive/conf/hive-site.xml",
        conf=spark_config,
        # todo do we need this after all?
        principal="analytics-research/an-airflow1002.eqiad.wmnet@WIKIMEDIA",
        keytab="/etc/security/keytabs/analytics-research/analytics-research.keytab",
    )