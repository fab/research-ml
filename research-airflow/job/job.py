# hackery
import os
import sys
os.environ['SPARK_HOME']="/usr/lib/spark2"
sys.path.append('/home/fab/.local/lib/python3.7/site-packages')

import argparse
import importlib
import json
import logging
from pyspark.sql import SparkSession

parser = argparse.ArgumentParser()

# we don't have types, so this is totally fine...?
parser.add_argument("--job-fn", required=True, help="entry point function (my_package.my_fun)")
parser.add_argument("--job-args", help="arguments to entry point function in json", type=json.loads)
parser.add_argument("--spark-config", help="spark config in json", type=json.loads)

if __name__ == "__main__":
    args = parser.parse_args()

    logging.info('config for spark context')
    logging.info(args.spark_config)
    builder = SparkSession.builder
    for k,v in args.spark_config.items():
        builder.config(k,v)
    spark = (builder
        .appName(args.job_fn)
        .getOrCreate())

    # import main method
    # done after the spark context is created, for spark import reasons
    module, method = args.job_fn.rsplit('.', 1)
    loaded_module = importlib.import_module(module)
    job_fn = getattr(loaded_module, method)

    logging.info(f"""module: {module}, method: {method}""")
    logging.info(f"""arguments: {args.job_args}""")
    res = job_fn(spark, **args.job_args)
    logging.info(f'{method} returned {res}')
