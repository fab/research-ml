import sys
from random import random
from operator import add

# hackery
import os
import sys
os.environ['SPARK_HOME']="/usr/lib/spark2"
sys.path.append('/home/fab/.local/lib/python3.7/site-packages')

from pyspark.sql import SparkSession

from research_transform.knowledge_gaps import common, gender, pages

def gender_gap(spark, wiki_data_snapshot, history_snapshot, target_wikis=None):

    wikidata_df = gender.create_wikidata_dataframe_for_gender_gap(spark, wiki_data_snapshot).cache()

    page_df = pages.create_page_dataframe_from_wikidata(
        spark, 
        wikidata_df, 
        wiki_data_snapshot, 
        history_snapshot, 
        target_wikis
    ).cache()

    gender_metrics = [
        gender.number_of_gender_item(wikidata_df),
        gender.number_of_gender_article_overall(page_df, wikidata_df),
        gender.number_of_gender_article_by_month(spark, page_df, history_snapshot),
        gender.number_of_gender_zero_ill(wikidata_df)
    ]

    for metric in gender_metrics:
        # print(metric.printSchema())
        metric.printSchema() 

spark_config = {
    # todo fix cluster mode errors
    # 'spark.submit.deployMode': 'cluster',
    # todo fix config issue that makes this fail
    # 'spark.jars.packages': 'org.apache.spark:spark-avro_2.11:2.4.4',
    'spark.driver.extraJavaOptions' : ' '.join('-D{}={}'.format(k, v) for k, v in {
        'http.proxyHost': 'webproxy.eqiad.wmnet',
        'http.proxyPort': '8080',
        'https.proxyHost': 'webproxy.eqiad.wmnet',
        'https.proxyPort': '8080',
    }.items()),
    'spark.sql.shuffle.partitions': 512,
    'spark.dynamicAllocation.maxExecutors': '10',
    'spark.executor.memory': '8g',
    'spark.executor.cores': '4',
    'spark.driver.memory': '4g',
    'spark.executorEnv.LD_LIBRARY_PATH': '/usr/lib/hadoop/lib/native',
}


if __name__ == "__main__":
    
    builder = SparkSession.builder             
    for k,v in spark_config.items():
        builder.config(k,v)

    spark = (builder
        .appName("knowledge gaps")
        .getOrCreate())
        
    print('running gender gap')
    gender_gap(spark, wiki_data_snapshot='2021-08-02', history_snapshot='2021-07')

    spark.stop()
