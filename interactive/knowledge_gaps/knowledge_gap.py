
# gender gap
#
# %%
# we load the research-transform from the repository

%load_ext autoreload
%autoreload 2

repo='/home/fab/code/research-ml'
import sys
sys.path.append(f"{repo}/research-transform")

#%%
from research_transform.knowledge_gaps import common, gender, pages

#%%
# used for wmf.wikidata_entity, wmf.wikidata_item_page_link
wiki_data_snapshot = '2021-08-02'
# used for wmf.mediawiki_page_history, wmf_raw.mediawiki_page
history_snapshot = '2021-07'
# target_wikis=['enwiki', 'frwiki', 'dewiki', 'zhwiki']
target_wikis=['fa']
# %%
#wikidata_df = spark.read.parquet('gender_wikidata_df.parquet')

def gender_gap(spark, wiki_data_snapshot, history_snapshot, target_wikis=None):

    wikidata_df = gender.create_wikidata_dataframe_for_gender_gap(spark, wiki_data_snapshot).cache()

    page_df = pages.create_page_dataframe_from_wikidata(
        spark,
        wikidata_df,
        wiki_data_snapshot,
        history_snapshot,
        target_wikis
    ).cache()

    gender_metrics = [
        gender.number_of_gender_item(wikidata_df),
        gender.number_of_gender_article_overall(page_df, wikidata_df),
        gender.number_of_gender_article_by_month(spark, page_df, history_snapshot),
        gender.number_of_gender_zero_ill(wikidata_df)
    ]

    for metric in gender_metrics:
        # print(metric.printSchema())
        metric.printSchema()

#%%

gender_gap(spark, '2021-08-02', '2021-07')

# %%
