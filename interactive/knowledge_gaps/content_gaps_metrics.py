# %%
from wmfdata.spark import get_session
spark = get_session(type='regular')

%load_ext autoreload
%autoreload 2

repo='/home/aikochou/research-ml'
import sys
sys.path.append(f"{repo}/research-transform")
# %%
from research_transform.knowledge_gaps import content_gaps, article_features, gender
import pyspark.sql.functions as F

mediawiki_snapshot = '2021-09'
wikidata_snapshot = '2021-10-04'

# %%
wikipedia_projects = ['cawiki', 'dewiki', 'enwiki', 'eswiki', 'itwiki']
wikipedia_pages = content_gaps.wikipedia_pages_df(spark, mediawiki_snapshot, wikidata_snapshot, wikipedia_projects) # ~13M pages 
# %%
wikipedia_pages = content_gaps.wikipedia_pages_df(spark, mediawiki_snapshot, wikidata_snapshot) # all wikis ~59M pages
# %%
wikidata_qitems = content_gaps.wikidata_qitems_df(spark, wikidata_snapshot) # around ~94M items
wikidata_properties = content_gaps.wikidata_properties(spark, wikidata_snapshot) # around ~1338M property-value pairs 
# %%
wikidata_properties.cache()
# %%
wikidata_qitems = content_gaps.append_is_human(wikidata_qitems, wikidata_properties)
wikidata_qitems = content_gaps.append_gender(wikidata_qitems, wikidata_properties)
wikidata_qitems = content_gaps.append_sexual_orientation(wikidata_qitems, wikidata_properties)
wikidata_qitems = content_gaps.append_partners(wikidata_qitems, wikidata_properties)
wikidata_qitems = content_gaps.append_geocoordinates(wikidata_qitems, wikidata_properties)
wikidata_qitems.printSchema()
# %%
wikidata_properties.unpersist()
# %%
wikitext = content_gaps.wikitext_df(spark, mediawiki_snapshot, wikipedia_projects)
wikitext_attributes = (wikitext
    .withColumn('article_features', 
        article_features.extract_wikitext_attributes('revision_text'))
    .drop('revision_text'))
# %%
content_gaps_items = (wikipedia_pages.alias('wp')
            .join(wikidata_qitems.alias('q'),
            F.col('wp.qitem_id') == F.col('q.qitem_id'), 'left')
            .select('wp.wiki_db', 'wp.page_id', 'wp.page_title', 'wp.create_timestamp', 'q.*'))
# %%
from pyspark.sql.functions import rand, when

content_gaps_items = (content_gaps_items
        .withColumn('year', F.substring('create_timestamp', 1, 4)) # for yearly aggregation
        .withColumn('quality_score', rand()) # random value from uniform distribution between 0 and 1 
        .withColumn('featured_article', when(rand() > 0.5, 1).otherwise(0)) # random value either 0 or 1
)
# %%
content_gaps_items.cache()
# %%
content_gaps_items.printSchema()
#root
# |-- wiki_db: string (nullable = true)
# |-- page_id: long (nullable = true)
# |-- page_title: string (nullable = true)
# |-- create_timestamp: string (nullable = true)
# |-- qitem_id: string (nullable = true)
# |-- is_human: boolean (nullable = true)
# |-- gender: string (nullable = true)
# |-- sexual_orientation: string (nullable = true)
# |-- partners: array (nullable = true)
# |    |-- element: string (containsNull = true)
# |-- geocoordinates: struct (nullable = true)
# |    |-- lat: string (nullable = true)
# |    |-- lon: string (nullable = true)
# |-- year: string (nullable = true)
# |-- quality_score: double (nullable = false)
# |-- featured_article: integer (nullable = false)

########################################################
# examples for computing metrics
# %%
metrics = (content_gaps_items
    .groupby(
        'wiki_db', 
        'year', 
        'is_human', 
        'gender', 
        'sexual_orientation', 
        'featured_article')
    .agg(
        F.count('*').alias('count'),
        F.sum('quality_score').alias('sum_quality_score')))
# %%
metrics.show() # 11m
# %%
metrics.cache()
# %%
gender_selection_metric = (metrics
    .groupby('wiki_db', 'is_human', 'gender')
    .agg(F.sum('count').alias('num_article')).alias('categories')
    .join((metrics
            .groupby('wiki_db', 'is_human')
            .agg(F.sum('count').alias('total_article'))).alias('gap'),
             (F.col('categories.wiki_db') == F.col('gap.wiki_db')) &
             (F.col('categories.is_human') == F.col('gap.is_human')), 'left')
    .select('categories.*', 'gap.total_article')
    .withColumn('percentage', F.col('num_article')/F.col('total_article')*100)
    .orderBy(['is_human', 'wiki_db', 'percentage'], ascending=False)
)
# %%
gender_selection_metric_yearly = (metrics
    .groupby('wiki_db', 'is_human', 'year', 'gender')
    .agg(F.sum('count').alias('num_article')).alias('categories')
    .join((metrics
            .groupby('wiki_db', 'is_human', 'year')
            .agg(F.sum('count').alias('total_article'))).alias('gap'),
             (F.col('categories.wiki_db') == F.col('gap.wiki_db')) &
             (F.col('categories.is_human') == F.col('gap.is_human')) & 
             (F.col('categories.year') == F.col('gap.year')), 'left')
    .select('categories.*', 'gap.total_article')
    .withColumn('percentage', F.col('num_article')/F.col('total_article')*100)
    .orderBy(['is_human', 'year', 'percentage'], ascending=False)
)
# %%
people_selection_metric = (metrics
    .groupby('wiki_db', 'is_human')
    .agg(F.sum('count').alias('num_article')).alias('categories')
    .join((metrics
        .groupby('wiki_db')
        .agg(F.sum('count').alias('total_article'))).alias('gap'),
         F.col('categories.wiki_db') == F.col('gap.wiki_db'), 'left')
    .select('categories.*', 'gap.total_article')
    .withColumn('percentage', F.col('num_article')/F.col('total_article')*100)
    .orderBy(['wiki_db', 'percentage'], ascending=False)
)
# %%
sexual_orientation_selection_metric = (metrics
    .groupby('wiki_db', 'is_human', 'sexual_orientation')
    .agg(F.sum('count').alias('num_article')).alias('categories')
    .join((metrics
            .groupby('wiki_db', 'is_human')
            .agg(F.sum('count').alias('total_article'))).alias('gap'),
             (F.col('categories.wiki_db') == F.col('gap.wiki_db')) &
             (F.col('categories.is_human') == F.col('gap.is_human')), 'left')
    .select('categories.*', 'gap.total_article')
    .withColumn('percentage', F.col('num_article')/F.col('total_article')*100)
    .orderBy(['is_human','wiki_db', 'percentage'], ascending=False)
)
# %%
gender_extent_metric = (metrics
    .groupby('wiki_db', 'is_human', 'gender')
    .agg(
        F.sum('sum_quality_score').alias('sum_quality_score'),
        F.sum('count').alias('num_article')
    )
    .withColumn('avg_quality_score', F.col('sum_quality_score')/F.col('num_article'))
    .orderBy(['is_human', 'wiki_db', 'num_article'], ascending=False)
)
# %%
sexual_orientation_extent_metric = (metrics
    .groupby('wiki_db', 'is_human', 'sexual_orientation')
    .agg(
        F.sum('sum_quality_score').alias('sum_quality_score'),
        F.sum('count').alias('num_article')
    )
    .withColumn('avg_quality_score', F.col('sum_quality_score')/F.col('num_article'))
    .orderBy(['is_human', 'wiki_db', 'num_article'], ascending=False)
)
# %%
gender_visibility_metric = (metrics
    .groupby('wiki_db', 'is_human', 'gender', 'featured_article')
    .agg(F.sum('count').alias('num_article')).alias('categories')
    .join((metrics
            .groupby('wiki_db', 'is_human')
            .agg(F.sum('count').alias('total_article'))).alias('gap'),
             (F.col('categories.wiki_db') == F.col('gap.wiki_db')) &
             (F.col('categories.is_human') == F.col('gap.is_human')), 'left')
    .select('categories.*', 'gap.total_article')
    .withColumn('percentage', F.col('num_article')/F.col('total_article')*100)
    .orderBy(['is_human', 'wiki_db', 'percentage'], ascending=False)  
)
# %%
sexual_orientation_visibility_metric = (metrics
    .groupby('wiki_db', 'is_human', 'sexual_orientation', 'featured_article')
    .agg(F.sum('count').alias('num_article')).alias('categories')
    .join((metrics
            .groupby('wiki_db', 'is_human')
            .agg(F.sum('count').alias('total_article'))).alias('gap'),
             (F.col('categories.wiki_db') == F.col('gap.wiki_db')) &
             (F.col('categories.is_human') == F.col('gap.is_human')), 'left')
    .select('categories.*', 'gap.total_article')
    .withColumn('percentage', F.col('num_article')/F.col('total_article')*100)
    .orderBy(['is_human', 'wiki_db', 'percentage'], ascending=False)  
)
# %%
# example for plotting graph
%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np

def plot_horizontal_bar_chart(results, category_names):
    labels = list(results.keys())
    data = np.array(list(results.values()))
    data_cum = data.cumsum(axis=1)
    category_colors = plt.get_cmap('plasma')(
        np.linspace(0.15, 0.85, data.shape[1]))

    fig, ax = plt.subplots(figsize=(10, 6))
    ax.invert_yaxis()
    ax.xaxis.set_visible(False)
    ax.set_xlim(0, np.sum(data, axis=1).max())

    for i, (colname, color) in enumerate(zip(category_names, category_colors)):
        widths = data[:, i]
        starts = data_cum[:, i] - widths
        rects = ax.barh(labels, widths, left=starts, height=0.5,
                        label=colname, color=color)

        r, g, b, _ = color
        text_color = 'white' if r * g * b < 0.5 else 'darkgrey'
        ax.bar_label(rects, label_type='center', color=text_color)
    ax.legend(ncol=len(category_names), bbox_to_anchor=(0, 1),
              loc='lower left', fontsize='large')

    return fig, ax
# %%
gender_labels = gender.add_gender_label(spark, content_gaps_items.select('gender').distinct(), wikidata_snapshot).toPandas()
gender_dict = {gender_labels[gender_labels.gender==g]['gender_label'].values[0].strip('"'): g for g in gender_labels['gender'].values}
gender_dict['female']
# %%
category_names = ['male', 'female', 'non-binary']
df = gender_selection_metric.toPandas()
results = {}
for wiki in df['wiki_db'].unique():
    results[wiki] = []
    for c in category_names:
        sub_df = df[(df.wiki_db==wiki)&(df.is_human==True)&(df.gender==gender_dict[c])]
        if sub_df.size:
            results[wiki].append(sub_df['percentage'].values[0])
        else:
            results[wiki].append(0.0)
results
# %%
plot_horizontal_bar_chart(results, category_names)
plt.show()
# %%