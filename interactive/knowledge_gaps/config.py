#-*- encoding: utf-8 -*-

import os
import types
from functools import reduce

_global_config = dict(
    mediawiki_snapshot = '2021-11',
    wikidata_snapshot = '2021-12-06',
    target_wikis = 'enwiki dewiki cawiki eswiki itwiki'
)

_local_mode = dict(
    run_local = True,
    hdfs_dir = 'hdfs://analytics-hadoop/user/aikochou/knowledge_gaps/',
    qids = 'qids_small.parquet', # or 'qids.parquet'
    properties = 'properties_small.parquet', # or 'properties.parquet'
    pages = 'pages_small.parquet' # or 'pages.parquet'
)

_external_file = dict(
    data_dir = './data',
    region_geoms = ('ne_10m_admin_0_map_units.geojson',
                    'https://github.com/geohci/wiki-region-groundtruth/raw/main/resources/ne_10m_admin_0_map_units.geojson'),
    country_properties_tsv = ('country_properties.tsv',
                    'https://github.com/geohci/wiki-region-groundtruth/raw/main/resources/country_properties.tsv'),
    country_aggregation_tsv = ('country_aggregation.tsv',
                    'https://github.com/geohci/wiki-region-groundtruth/raw/main/resources/country_aggregation.tsv'),
    region_qids_tsv = ('base_regions_qids.tsv',
                'https://github.com/geohci/wiki-region-groundtruth/raw/main/resources/base_regions_qids.tsv'),
    gender_categories_tsv = ('gender_categories.tsv',
                        'https://github.com/AikoChou/wikimedia-research-2021/raw/main/knowledge-gap-metrics/gender_categories.tsv'),
    sexual_orientation_categories_tsv = ('sexual_orientation_categories.tsv',
                                    'https://github.com/AikoChou/wikimedia-research-2021/raw/main/knowledge-gap-metrics/sexual_orientation_categories.tsv'),
    time_properties_tsv = ('time_properties.tsv',
                        'https://github.com/AikoChou/wikimedia-research-2021/raw/main/knowledge-gap-metrics/time_properties.tsv'),
    country_continent_json = ('country_continent.json', # if failed..
                        'https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes/raw/master/all/all.json')
)

Config = types.SimpleNamespace

def _inherit(base, child):
    ret = dict(base)  # shallow copy
    for k, v in child.items():
        if k in ret:
            if isinstance(v, list):
                v = ret[k] + v
            elif isinstance(v, dict):
                v = dict(ret[k], **v)
        ret[k] = v
    return ret

def get_config():
    cfg = Config( **reduce(
        _inherit, [_global_config, _local_mode, _external_file]))
    return cfg