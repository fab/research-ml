#%%
import os

os.environ['SPARK_HOME']="/usr/lib/spark2"
os.environ['JAVA_HOME']="/usr/lib/jvm/java-8-openjdk-amd64/jre"
# this is required. setting the spark.pyspark.python conf doesn't suffice.
os.environ["PYSPARK_PYTHON"] = "./venv/bin/python"

from pyspark.sql.dataframe import DataFrame
from pyspark.sql import Row, SparkSession, Window
import pyspark.sql.functions as F
import pyspark.sql.types as T

# todo, ci should build the conda env, configurable using a branch/sha?
main_conda_env = "hdfs://analytics-hadoop/user/aikochou/python/conda-env-knowledge-gap.tgz"

default_spark_config = {
    # "spark.scheduler.mode": "FAIR",
    "spark.driver.memory": "4g",
    "spark.dynamicAllocation.maxExecutors": 64,
    "spark.executor.memory": "8g",
    "spark.executor.cores": 1,
    "spark.sql.shuffle.partitions": 512,
    "spark.yarn.executor.memoryOverhead": 2048,
    "spark.jars.packages": "org.apache.spark:spark-avro_2.11:2.4.4",
    "spark.pyspark.driver.python": "python",
    "spark.pyspark.python": "./venv/bin/python"
}

def create_context(
    spark_config=default_spark_config,
    conda_env=main_conda_env):
    """
    spark_config: dict of configuration to pass to spark
    conda_env (optional): path (local,hdfs) to packed conda environment. run `conda pack --ignore-editable-packages` to generate a custom env.
    """
    spark_config["spark.yarn.dist.archives"] = f"{conda_env}#venv"

    builder = SparkSession.builder
    for k,v in spark_config.items():
        builder.config(k,v)
    return (builder
        .master('yarn')
        .appName("knowledge_gap")
        .getOrCreate())
