# %%
%load_ext autoreload
%autoreload 2

import os
import sys
import argparse
import subprocess

# we load the research-transform from the repository
repo = subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE).communicate()[0].rstrip().decode('utf-8')
# repo=''git rev-parse --show-toplevel
print(f"{repo} added to python path")
sys.path.append(f"{repo}/research-transform")

# %%
import config
cfg = config.get_config()

if cfg.run_local:
    from wmfdata.spark import get_session
    spark = get_session(type='local')
else:
    import spark as spark_script
    spark = spark_script.create_context()

# %%
import pyspark.sql.functions as F
from research_transform.knowledge_gaps import util, func

# external libraries for geography gap
import reverse_geocoder as rg
from shapely.geometry import shape, Point

# load in external data
gender_categories = util.get_categories(os.path.join(cfg.data_dir, cfg.gender_categories_tsv[0]))
sexual_orientation_categories = util.get_categories(os.path.join(cfg.data_dir, cfg.sexual_orientation_categories_tsv[0]))
country_continent = util.get_country_continent(os.path.join(cfg.data_dir, cfg.country_continent_json[0]))
region_categories = util.get_qid_to_region(os.path.join(cfg.data_dir, cfg.region_qids_tsv[0]), 
                                os.path.join(cfg.data_dir, cfg.country_aggregation_tsv[0]))
region_shapes = util.get_region_data(region_categories, os.path.join(cfg.data_dir, cfg.region_geoms[0]))
region_properties = util.get_properties(os.path.join(cfg.data_dir, cfg.country_properties_tsv[0]))
time_properties = util.get_properties(os.path.join(cfg.data_dir, cfg.time_properties_tsv[0]))

@F.udf(returnType="string")
def point_in_country_udf(lon, lat):
    # TODO - the udf is still here because it uses `region_shapes` above
    """Determine which region contains a lat-lon coordinate.
    
    Depends on shapely library and region_shapes object, which contains 
    a dictionary mapping QIDs to shapely geometry objects.
    """
    try:
        pt = Point(lon, lat)
        for qid in region_shapes:
            if region_shapes[qid].contains(pt):
                return qid
        return None
    except Exception:
        return None


def content_gap_features(spark, qids, properties):
    properties.cache()
    # create dataframes for labeling
    qid_to_gender = spark.createDataFrame(gender_categories, ['qid', 'gender'])
    qid_to_sexual_orientation = spark.createDataFrame(sexual_orientation_categories, ['qid', 'sexual_orientation'])
    qid_to_region = spark.createDataFrame(region_categories.items(), ['qid', 'region'])
    cc_to_continent = spark.createDataFrame(country_continent, ['name', 'cc', 'continent', 'sub_continent'])
    
    region_properties_list = [p[0] for p in region_properties]
    time_properties_list = [p[0] for p in time_properties]
    
    # append content gap features
    qitems = func.append_is_human(qids, properties)
    qitems = func.append_gender_gap(qitems, properties, qid_to_gender)
    qitems = func.append_sexual_orientation_gap(qitems, properties, qid_to_sexual_orientation)
    qitems = func.append_geography_gap(qitems, properties, qid_to_region, 
                                       cc_to_continent, region_properties_list, 
                                       point_in_country_udf)
    qitems = func.append_time_gap(qitems, properties, time_properties_list)
    properties.unpersist()
    return qitems

# %%
if cfg.run_local:
    """
    small dataset:
        * n_qid: 3781
            - gender: 2675, sexual orientation: 785, geo: 3567, time: 3239
        * n_properties: 169262
        * n_pages(=result_df.count): 13657
            - en: 2871, de: 2200, ca: 3781, it: 2174, es: 2631
        * run time: 4m21s

    dataset:
        * n_qid: 19513
            - gender: 10742, sexual orientation: 785, geo: 17824, time: 14434
        * n_properties: 700989
        * n_pages(=result_df.count): 66737
            - en: 13740, de: 10339, ca: 19513, it: 12556, es: 10589
        * run time: 21m5s
    """
    qids = spark.read.parquet(os.path.join(cfg.hdfs_dir, cfg.qids))
    properties = spark.read.parquet(os.path.join(cfg.hdfs_dir, cfg.properties))
    pages = spark.read.parquet(os.path.join(cfg.hdfs_dir, cfg.pages))

else:
    mediawiki_snapshot = cfg.mediawiki_snapshot
    wikidata_snapshot = cfg.wikidata_snapshot
    projects = cfg.target_wikis.split(' ')
    
    qids = func.wikidata_relevant_qitems_df(spark, wikidata_snapshot, mediawiki_snapshot, projects)
    properties = func.wikidata_relevant_properties(spark, wikidata_snapshot, qids)
    pages = func.wikipedia_pages_df(spark, mediawiki_snapshot, wikidata_snapshot, projects)
    

df = func.get_one_dataframe(pages, content_gap_features(spark, qids, properties))
df.printSchema()

# %%

# save dataframe to parquet
destination = 'knowledge_gaps/output.parquet'
num_partitions = 1

(df
    .coalesce(num_partitions)
    .write.partitionBy("wiki_db")
    .mode("overwrite")
    .parquet(destination)
)