# research-interactive

## Jupyter

The `research-transform` library can be installed from a jupyter notebook via
```
!pip install -e 'git+https://gitlab.wikimedia.org/FabianKaelin/research-ml.git@main#egg=research_transform&subdirectory=research-transform'
```

The command above install from the `main` branch, you can change to a different branch if desired.

## IPython 

Contributing code to a repository (like research-ml) from jupyter is not very convenient given the focus on notebooks. An alternative setup is to use VScode with the  Remote-SSH, Python and Jupyter extensions.
- run `Remote-SSH: Connect to host` with a stat machine
- git clone a repository on the stat machine
- open that folder in the remote VScode session
- start in interactive ipython session using the jupyter kernels on the stat machine with `Jupyter: Create interactive window`. 
- use the python snippets in `interactive` as needed

You can also write code in the IDE, and at the same time use the repository interactively either in a jupytyer notebook or an interactive ipython session. The snippet below causes a module to be reloaded each time it is used, so any changes done in the IDE will be picked up by the interactive session automatically.

```
%load_ext autoreload
%autoreload 2

repo='/home/fab/code/research-ml'
import sys
sys.path.append(f"{repo}/research-transform")
```
