#%%

# Experiment with Polars 

# Fast non-distributed dataframe library, 
# suitable for the beefy stat machines

# https://github.com/pola-rs/polars

!pip3 install polars
!pip3 install fsspec

#%%
import base64
from io import BytesIO
from PIL import Image

import polars as pl 
#%%
# no compression support?
# x = pl.read_parquet("hdfs://analytics-hadoop/wmf/data/wmf/webrequest/webrequest_source=text/year=2021/month=9/day=10/hour=12/000255_0")
# x = pl.read_parquet("hdfs://analytics-hadoop/user/fab/moma.parquet")
# x = pl.read_parquet("hdfs://analytics-hadoop/user/fab/images")
# x = pl.read_csv("hdfs://analytics-hadoop/user/fab/images/datasets/test_val/image_pixels/part-00004-68527a52-4fcf-4098-84be-0a6724c59119-c000.csv.gz")

pixels = pl.read_csv(
    file="/home/fab/code/research-datasets/polars.csv",
    has_headers=False,
    sep='\t',
    new_columns=["url", "b64_bytes", "meta"]
)

# x = pl.read_parquet("/home/fab/moma.parquet/part-00009-15df927a-2092-4d2c-b4b6-8cdf5b92e44c-c000.snappy.parquet")

#%%

def parse_image(b64bytes):    
    pil_image = Image.open(BytesIO(base64.b64decode(b64bytes)))
    return pil_image

pixels["image"] = pixels["b64_bytes"].apply(parse_image)

# %%

for i in pixels["image"][:5]:
    display(i)

# %%
