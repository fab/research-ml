# research-ml

This repository serves multiple purposes
- research-transform: library for sharing and reusing code for research at wmf
- research-airflow: tooling for running orchestrated machine learning pipelines in the data engineering infrastructure

Instead of splitting different functionality into different repos prematurely, this will be a mono repo for now. 

