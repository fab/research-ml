
from setuptools import (
    find_packages,
    setup
)

spark_version="2.4.4"

setup(
    name="research_transform",
    version="0.1",
    install_requires=[                
        f"pyspark[sql]=={spark_version}",
        "pillow",
        "wmfdata @ git+https://github.com/wikimedia/wmfdata-python.git@release",
        "Shapely",
        "reverse-geocoder"
    ],
    packages=find_packages(),
    python_requires=">=3",
    author="Wikimedia Foundation Research team",
    license="BSD 3-Clause"
)
