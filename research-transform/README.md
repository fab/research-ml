# research-transform

Library for sharing and reusing code for research at wmf, with a focus on pipelines that transform a wmf data stream for a machine learning algorithm.
    - graphs revision graph, diffs 
    - redirect resolution, link graph between articles
    - topic modelling
    - retrieve data from swift 
    - ...

