import logging
import os
import skein
import time
import queue

os.environ['HADOOP_CONF_DIR']='/etc/hadoop/conf'
import pydoop.hdfs as hdfs


def deploy_kv_import(
    import_data_dir,
    tikv_urls,
    n_importers=None,
    ):
    """
    import_data_dir: hdfs directory with parquet files with key/value pairs
    client_urls: tikv pd server urls
    """

    kv_store_import_cmd = f"""
        chmod +x kv_store_import && \
        ./kv_store_import \
            --kv-file kv_pairs.parquet \
            --client-urls {" ".join(tikv_urls)} || \
        echo "kv_store_import failed" && \
        sleep 60
        """


    hdfs_files = hdfs.lsl(import_data_dir)
    total_bytes = sum([f['size'] for f in hdfs_files])
    logging.info(f"""{import_data_dir}: {len(hdfs_files)} files, {total_bytes/1e9}GB""")

    services = {}
    instances = 1 if not n_importers else 0
    for i, hdfs_file in enumerate(hdfs_files):
        services[f"kv_import_{i}"] = skein.Service(
            script=kv_store_import_cmd,
            files={
                "kv_store_import": 'hdfs://analytics-hadoop/user/fab/rust/tikv/kv_store_import',
                "kv_pairs.parquet": hdfs_file['name']
            },
            instances=instances,
            #TODO test if tikv client uses threads with >1 cores
            resources=skein.Resources(memory='4 GiB', vcores=1))

    spec = skein.ApplicationSpec(
        name=f"kv-import",
        services=services
    )

    with skein.Client(log_level='debug') as client:

        app_client = client.submit_and_connect(spec)

        logging.info(f'started kv-import applications')

        if n_importers:
            to_run = list(services.keys())
            while len(to_run)>0:
                print(f"still to run {len(to_run)}")
                if len(app_client.get_containers()) < n_importers:
                    next = to_run.pop()
                    print(f"deploying importer {next}")
                    app_client.add_container(service=next)
                else:
                    time.sleep(1)



    logging.info("import done")
    return app_client

