import logging
import skein
import time

# tikv binaries and config on hdfs
# todo config for pd-server
# todo, add these via files argument
hdfs_binaries_dir='/user/fab/rust/tikv'

# todo, ci should build the conda env, configurable using a branch/sha?
conda_env = "hdfs://analytics-hadoop/user/fab/python/env-research-ml.tar.gz"

files = { "env": conda_env }

# todo binaries should be passed as skein.File or files = {} ?
setup_cmd = f"""
mkdir bin && \
hdfs dfs -get {hdfs_binaries_dir}/* && \
chmod +x bin/*"""

pd_cluster_cmd="""
echo "setup python" && \
source env/bin/activate && \
{ python - << EOF

import os
import logging
import socket
import time
import tempfile
skein_dir = tempfile.TemporaryDirectory()
os.environ['SKEIN_CONFIG']=skein_dir.name
import skein
import subprocess
client = skein.Client(log_level='debug')
app_client = client.connect(skein.properties.application_id)
client_port = "2379"
peer_port = "2380"
tikv_port = "20160"
log_level='error'
name = os.environ['PD_NAME']
n_pd = int(os.environ['N_PD'])
host_ip = socket.gethostbyname(socket.gethostname())
client_urls = 'http://'+host_ip+':'+client_port
peer_urls = 'http://'+host_ip+':'+peer_port
print("update skein.kv with "+host_ip)
app_client.kv.put(name, bytes(host_ip,"utf-8"))
registered_hosts = []
# wait until all hosts started, including the python env skein client for the deploy
while len(registered_hosts)!=n_pd:
    print("wait for pd servers to initialize, current set "+str(registered_hosts))
    registered_hosts = app_client.kv.get_prefix("pd_")
    time.sleep(1)
print("pd servers containers initialized")
first_name, first_ip = list(registered_hosts.items())[0]
pd_server_cmd=['./bin/pd-server', '--name='+name, '--L='+log_level, '--data-dir=tikv_data', '--client-urls='+client_urls, '--peer-urls='+peer_urls]
print(pd_server_cmd)
if name==first_name:
    #start pd cluster
    print("initalize cluster with server: name: "+first_name+", ip: "+first_ip.decode("utf-8"))
    cmd = pd_server_cmd + ['--initial-cluster='+first_name+"=http://"+first_ip.decode("utf-8")+':'+peer_port]
    print(cmd)
    attempt=0
    max_retry=3
    while attempt<max_retry:
        try:
            subprocess.run(cmd,check=True)
        except subprocess.CalledProcessError as e:
            print(e.output)
            time.sleep(3)
            attempt+=1
else:
    print("join cluster with with server: name: "+name+", ip: "+host_ip)
    print("pseudo cascading deploy")
    wait = list(registered_hosts.keys()).index(name) * 5
    print("wait "+str(wait))
    time.sleep(wait)
    cmd = pd_server_cmd + ['--join=http://'+first_ip.decode("utf-8")+':'+client_port]
    print(cmd)
    attempt=0
    max_retry=3
    while attempt<max_retry:
        try:
            subprocess.run(cmd,check=True)
        except subprocess.CalledProcessError as e:
            print(e.output)
            time.sleep(3)
            attempt+=1

EOF
} && \
echo "done waiting for initial cluster" """

log_level='error' # error info debug

client_port = "2379"
peer_port = "2380"
tikv_port = "20160"

def deploy_tikv(
    n_pd, n_tikv,
    pd_cores=8, pd_memory='4 GiB',
    tikv_cores=8, tikv_memory='8 GiB'):

    pd_server_cmd = f"""
        {setup_cmd} && \
        {pd_cluster_cmd} || \
        echo "pd-server deploy failed" && \
        sleep 60
        """

    tikv_server_cmd = f"""
        {setup_cmd} && \
        echo "running tikv-server connected to pd-servers at $PD_ENDPOINTS" && \
        ./bin/tikv-server \
            --pd-endpoints="$PD_ENDPOINTS" \
            --addr="`hostname -i`:{client_port}" \
            --data-dir=tikv1 \
            --config ./config/tikv_config.toml \
            --log-level={log_level} || \
        echo "tikv-server deploy failed" && \
        sleep 600
        """

    services = {
        "pd-server": skein.Service(
            # TODO xxx
            script=pd_server_cmd,
            files=files,
            instances=0,
            resources=skein.Resources(memory=pd_memory, vcores=pd_cores)
        ),
        "tikv-server": skein.Service(
            script=tikv_server_cmd,
            # files=files,
            instances=0,
            resources=skein.Resources(memory=tikv_memory, vcores=tikv_cores)
        )
    }

    spec = skein.ApplicationSpec(
        name=f"tikv",
        services=services
    )

    with skein.Client(log_level='debug') as client:

        app_client = client.submit_and_connect(spec)

        logging.info(f'deploying {n_pd} pd endpoints')
        for i in range(n_pd):
            app_client.add_container(
                service="pd-server",
                env={'PD_NAME': f'pd_{i}', 'N_PD': str(n_pd) })

        initialized_pd_hosts = {}
        while len(initialized_pd_hosts)!=n_pd:
            logging.info("wait for pd servers to initialize, current set "+str(initialized_pd_hosts.keys()))
            initialized_pd_hosts = app_client.kv.get_prefix("pd_")
            time.sleep(1)

        eps = []
        for host in initialized_pd_hosts.values():
            eps.append(f'{host.decode("utf-8")}:{client_port}')
        pd_endpoints = ",".join(eps)
        logging.info(f'available pd-server endpoints: {pd_endpoints}')
        # giving the pd servers time to boot
        time.sleep(10)
        logging.info(f'deploying {n_tikv} tikv servers')
        for i in range(n_tikv):
            app_client.add_container(service="tikv-server", env={'PD_ENDPOINTS': pd_endpoints })


    logging.info("deploy done. tikv cluster runn")
    return app_client, eps

