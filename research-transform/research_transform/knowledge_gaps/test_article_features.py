import article_features
import mwparserfromhell

import unittest

class TestArticleFeatures(unittest.TestCase):
    def test_extract_wikilinks(self):
        wikitext = """
        This is an example of wikilinks. Represents an internal wikilink, like [[text to display|title of the linked page]].
        """
        wikicode = mwparserfromhell.parse(wikitext)
        ret = article_features.extract_wikilinks(wikicode)
        self.assertIsInstance(ret, list)
        self.assertEqual(len(ret), 1)
        self.assertIn('text_to_display', ret)

    def test_extract_number_references(self):
        wikitext = """
        For a citation to appear in a footnote, it needs to be enclosed in "ref" tags. <ref>citation</ref>
        A short form appears using Shortened footnote template. {{sfn}}
        """
        wikicode = mwparserfromhell.parse(wikitext)
        ret = article_features.extract_number_references(wikicode)
        self.assertIsInstance(ret, int)
        self.assertEqual(ret, 2)

    def test_extract_headings(self):
        wikitext = """
        This is an example of headings.\n== Heading 2 ==\nRepresents a section heading in wikicode.
        \n=== Heading 3 ===\nHeadings follow a six-level hierarchy, starting at 1 and ending at 6. 
        """
        wikicode = mwparserfromhell.parse(wikitext)
        ret = article_features.extract_headings(wikicode)
        self.assertIsInstance(ret, list)
        self.assertEqual(len(ret), 2)
        self.assertIn('Heading 2', ret)
        self.assertIn('Heading 3', ret)

    def test_extract_templates(self):
        wikitext = """
        This is an example of templates. Represents a template in wikicode, like {{tpl}}.
        """
        wikicode = mwparserfromhell.parse(wikitext)
        ret = article_features.extract_templates(wikicode)
        self.assertIsInstance(ret, list)
        self.assertEqual(len(ret), 1)
        self.assertIn('tpl', ret)

if __name__ == '__main__':
    unittest.main()