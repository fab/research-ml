import pyspark.sql.functions as F
import reverse_geocoder as rg

@F.udf(returnType="int")
def get_year_udf(time):
    """Get year from a timestamp. The year is always 
    signed and padded to have between 4 and 16 digits
    """
    year = None
    if time:
        year = int(time[0] + time[1:].split('-')[0])
    return year

@F.udf(returnType="struct<name:string,cc:string,admin1:string,admin2:string>")
def reverse_geocode_udf(lon, lat):
    """Reverse Geocoder takes a latitude and longitude 
    coordinate and returns the nearest town/city, 
    country code, and the administrative 1 & 2 regions.
    """
    try:
        res = rg.search((lat, lon))
        return {'name': res[0]['name'],
                'cc': res[0]['cc'], 
                'admin1': res[0]['admin1'],
                'admin2': res[0]['admin2']}
    except Exception:
        return None

def wikidata_relevant_qitems_df(spark, wikidata_snapshot, mediawiki_snapshot, projects=None):
    """Create a DataFrame with single column named `qitem_id`, containing 
    Wikidata item IDs that have at least one Wikipedia sitelink from `projects`.

    Parameters:
        * spark - SparkSession
        * wikidata_snapshot - Hive partition (YYYY-MM-DD) for `wikidata_item_page_link`
        * mediawiki_snapshot - Hive partition (YYYY-MM) for `mediawiki_project_namespace_map`
        * projects - a list of Wikipedia projects. If not specified, the default is all Wikipedia projects

    Returns: DataFrame        
        |-- qitem_id: string
    """
    query = f"""    
        SELECT DISTINCT(dbname) AS wiki_db
        FROM wmf_raw.mediawiki_project_namespace_map
        WHERE
          snapshot = '{mediawiki_snapshot}'
          AND hostname LIKE '%.wikipedia.org'
    """
    relevant_wikis = spark.sql(query)
    if projects:
        relevant_wikis = relevant_wikis.where(F.col('wiki_db').isin(projects))

    relevant_wikis.createOrReplaceTempView('relevant_wikis')
    query = f"""
        SELECT DISTINCT(item_id) AS qitem_id
        FROM wmf.wikidata_item_page_link wd
        INNER JOIN relevant_wikis db
          ON (wd.wiki_db = db.wiki_db)
        WHERE
          snapshot = '{wikidata_snapshot}'
          AND page_namespace = 0
    """
    df = spark.sql(query)
    return df


def wikidata_relevant_properties(spark, wikidata_snapshot, relevant_qids):
    """Given Wikidata item IDs `relevant_qids`, explode their Wikidata 
    entity data to one property-value per row.

    Parameters:
        * spark - SparkSession
        * wikidata_snapshot - Hive partition (YYYY-MM-DD) for `wikidata_entity`
        * relevant_qids - a DataFrame with single column named `qitem_id`

    Returns: DataFrame
        |-- qitem_id: string
        |-- property: string
        |-- value: string
    """
    relevant_qids.createOrReplaceTempView('relevant_qids')
    query = f"""
    SELECT id AS qitem_id, claim.mainSnak.property, claim.mainsnak.datavalue.value
    FROM wmf.wikidata_entity w
    INNER JOIN relevant_qids q
      ON (w.id = q.qitem_id)
    LATERAL VIEW explode(claims) AS claim
    WHERE w.typ = 'item'
        AND w.snapshot = '{wikidata_snapshot}'
    """
    return spark.sql(query)


def wikipedia_pages_df(spark, mediawiki_snapshot, wikidata_snapshot, projects=None):
    """Create a DataFrame of Wikipedia articles, containing page ID, 
    page title, correspond Wikidata item ID, Wikipedia project, and 
    article creation time

    Parameters:
        * spark - SparkSession
        * mediawiki_snapshot - Hive partition (YYYY-MM) for `mediawiki_page`
        * wikidata_snapshot - Hive partition (YYYY-MM-DD) for `wikidata_item_page_link`
        * projects - a list of Wikipedia projects. If not specified, the default is all Wikipedia projects

    Returns: DataFrame
        |-- wiki_db: string
        |-- page_id: long
        |-- page_title: string
        |-- qitem_id: string
        |-- create_timestamp: string
    """
    query = f"""
        SELECT p.wiki_db, p.page_id, p.page_title,
            wipl.item_id AS qitem_id,  
            mp.page_first_edit_timestamp AS create_timestamp
        FROM wmf_raw.mediawiki_page p
        INNER JOIN wmf.mediawiki_page_history mp
        ON p.page_id = mp.page_id
            AND p.wiki_db = mp.wiki_db
        LEFT JOIN wmf.wikidata_item_page_link wipl
        ON p.wiki_db = wipl.wiki_db
            AND p.page_id = wipl.page_id
        WHERE p.snapshot='{mediawiki_snapshot}'
            AND p.page_namespace=0
            AND p.page_is_redirect=0
            AND mp.snapshot='{mediawiki_snapshot}'
            AND wipl.snapshot = '{wikidata_snapshot}'
            AND wipl.page_namespace = 0
        """
    df = spark.sql(query).dropDuplicates()
    if projects:
        df = df.where(F.col('wiki_db').isin(projects))
    return df


def append_is_human(qitems, wikidata_properties):
    """Add a boolean column `is_human` to DataFrame `qitems`. 
    True if the Wikidata item has property `instance of` (P31) 
    and contains value `human` (Q5).

    Parameters:
        * qitems - column `qitem_id` is required
        * wikidata_properties - property-value for the Wikidata items in `qitems`
    """
    wikidata_property = (wikidata_properties
        .filter(F.col('property') == 'P31')
        .withColumn('value', F.json_tuple('value', 'id'))
        .groupby('qitem_id')
        .agg(F.collect_list('value').alias('instance_of'))
        .withColumn('is_human', F.array_contains(F.col('instance_of'), 'Q5')) # Q5: human
        .select('qitem_id', 'is_human'))
    df = (qitems.alias('q')
        .join(wikidata_property.alias('p'), F.col('q.qitem_id') == F.col('p.qitem_id'), 'left')
        .select('q.*', 'p.is_human'))
    df = df.fillna(False, ['is_human'])
    return df


def append_gender_gap(qitems, wikidata_properties, qid_to_gender):
    """Add a column `gender_gap` to DataFrame `qitems`. Null if the Wikidata 
    item doesn't have property `P31 (sex or gender)`.

    Parameters:
        * qitems - column `qitem_id` is required
        * wikidata_properties - property-value for the Wikidata items in `qitems`
        * qid_to_gender - a DataFrame used for labeling, containing columns `qid` and `gender`

    Schema:
    |-- gender_gap: array
    |    |-- element: struct
    |    |    |-- gender_qid: string
    |    |    |-- gender: string  
    """
    gender_item = (wikidata_properties
        .filter(F.col('property') == 'P21')
        .withColumn('gender_qid', F.json_tuple('value', 'id'))
        .join(qid_to_gender.alias('q'), F.col('q.qid') == F.col('gender_qid'), 'left')
        .withColumn('gender_gap', F.struct(F.col('gender_qid'), F.col('q.gender')))
        .select('qitem_id', 'gender_gap')
        .groupby('qitem_id')
        .agg(F.collect_set('gender_gap').alias('gender_gap')))
    df = (qitems.alias('q')
        .join(gender_item.alias('g'), F.col('q.qitem_id') == F.col('g.qitem_id'), 'left')
        .select('q.*', 'g.gender_gap'))
    return df


def append_sexual_orientation_gap(qitems, wikidata_properties, qid_to_label):
    """Add a column `sexual_orientation_gap` to DataFrame `qitems`. Null if 
    the Wikidata item does not have property `P91 (sexual orientation)`.

    Parameters:
        * qitems - column `qitem_id` is required
        * wikidata_properties - property-value for the Wikidata items in `qitems`
        * qid_to_sexual_orientation - DataFrame used for labeling, containing 
            columns `qid` and `sexual_orientation`

    Schema:
    |-- sexual_orientation_gap: array
    |    |-- element: struct
    |    |    |-- sexual_orientation_qid: string
    |    |    |-- sexual_orientation: string
    """
    sexual_orientation_item = (wikidata_properties
        .filter(F.col('property') == 'P91')
        .withColumn('sexual_orientation_qid', F.json_tuple('value', 'id'))
        .join(qid_to_label.alias('q'), F.col('q.qid') == F.col('sexual_orientation_qid'), 'left')
        .withColumn('sexual_orientation_gap', F.struct(F.col('sexual_orientation_qid'), F.col('q.sexual_orientation')))
        .select('qitem_id', 'sexual_orientation_gap')
        .groupby('qitem_id')
        .agg(F.collect_set('sexual_orientation_gap').alias('sexual_orientation_gap')))
    df = (qitems.alias('q')
        .join(sexual_orientation_item.alias('s'), F.col('q.qitem_id') == F.col('s.qitem_id'), 'left')
        .select('q.*', 's.sexual_orientation_gap'))
    return df


def get_geolocated_qitem(wikidata_properties, qid_to_region, point_in_country, cc_to_continent):
    """Extract the geocoordinates of the Wikidata item from property 
    `P625 (coordinate location)` and append more metadata from external 
    sources.

    * reverse geocoder: adds `name`, `cc` (ISO 3166-1 alpha-2 country code), 
        `admin1`, `admin2` (see https://github.com/thampiman/reverse-geocoder)    
    
    * region scheme: adds `region_id` and `region` using shapely library 
        to associate the Wikidata item with the predefined regions.

    * adds `continent` and `sub_continent`

    Schema:
    |-- geolocated: array
    |    |-- element: struct
    |    |    |-- geocoordinates: struct
    |    |    |    |-- lat: float 
    |    |    |    |-- lon: float 
    |    |    |-- reverse_geocode: struct 
    |    |    |    |-- name: string 
    |    |    |    |-- cc: string 
    |    |    |    |-- admin1: string 
    |    |    |    |-- admin2: string 
    |    |    |-- region_scheme: struct 
    |    |    |    |-- region_qid: string 
    |    |    |    |-- region: string 
    |    |    |-- continent: string 
    |    |    |-- sub_continent: string 
    """
    geolocated = (wikidata_properties
        .filter(F.col('property') == 'P625')
        .select(F.col('qitem_id'), F.json_tuple(F.col('value'), 'latitude', 'longitude'))
        .withColumn('geocoordinates', F.struct(
                                        F.col('c0').cast('float').alias('lat'), 
                                        F.col('c1').cast('float').alias('lon')))
        .withColumn('reverse_geocode', reverse_geocode_udf(
                                    F.col('geocoordinates.lon'), 
                                    F.col('geocoordinates.lat')))
        .withColumn('region_qid', point_in_country(
                                    F.col('geocoordinates.lon'), 
                                    F.col('geocoordinates.lat')))
        .drop('c0', 'c1')
        .join(qid_to_region.alias('q'), F.col('q.qid') == F.col('region_qid'), 'left')
        .join(cc_to_continent.alias('c'), F.col('c.cc') == F.col('reverse_geocode.cc'), 'left')
        .withColumn('region_scheme', F.struct(F.col('region_qid'), F.col('q.region')))
        .withColumn('geolocated', F.struct(F.col('geocoordinates'), F.col('reverse_geocode'), 
                        F.col('region_scheme'), F.col('c.continent'), F.col('c.sub_continent')))
        .select('qitem_id', 'geolocated')
        .groupby('qitem_id')
        .agg(F.collect_set('geolocated').alias('geolocated')))
    return geolocated
    

def get_region_qitem(wikidata_properties, qid_to_region, region_properties_list):
    """Extract the value of the Wikidata item from properties in a 
    select set of properties like `place of birth` or `country of citizenship`
    and matchs values to the list of regions. 

    Schema:
    |-- property_based: array
    |    |-- element: struct
    |    |    |-- property: string
    |    |    |-- value: string
    |    |    |-- region: string
    """
    region_item = (wikidata_properties
    .filter(F.col('property').isin(region_properties_list))
    .withColumn('value', F.json_tuple('value', 'id'))
    .join(qid_to_region.alias('q'), F.col('q.qid') == F.col('value'), 'left')
    .withColumn('property_based', F.struct(F.col('property'), 
                                            F.col('value'), 
                                            F.col('q.region')))
    .select('qitem_id', 'property_based')
    .groupby('qitem_id')
    .agg(F.collect_set('property_based').alias('property_based')))
    return region_item


def append_geography_gap(qitems, wikidata_properties, qid_to_region, cc_to_continent, region_properties_list, point_in_country):
    """Add a column `geography_gap` to DataFrame `qitems`.

    Geography gap contains two parts, one is geolocated items based on 
    coordinate locations, another is region items based on a select set 
    of properties.
    """
    geolocated_item = get_geolocated_qitem(wikidata_properties, qid_to_region, point_in_country, cc_to_continent)
    region_item = get_region_qitem(wikidata_properties, qid_to_region, region_properties_list)
    df = (qitems.alias('q')
        .join(geolocated_item.alias('g'), F.col('q.qitem_id') == F.col('g.qitem_id'), 'left')
        .join(region_item.alias('r'), F.col('q.qitem_id') == F.col('r.qitem_id'), 'left')
        .select('q.*', 'g.geolocated', 'r.property_based')
        .withColumn('geography_gap', F.struct(F.col('geolocated'), F.col('property_based')))
        .withColumn('geography_gap', F.when(F.col('geography_gap.geolocated').isNull() & F.col('geography_gap.property_based').isNull(), None).otherwise(F.col('geography_gap')))
        .drop('geolocated', 'property_based'))
    return df


def append_time_gap(qitems, wikidata_properties, time_properties_list):
    """Add a column `time_gap` to DataFrame `qitems`.

    Extract the timestamps of the Wikidata item from properties in a 
    select set of properties like `date of birth` or `publication date` 
    and extract the year to a new integar column `year` and a boolean 
    column `before_common_era`

    Schema:
    |-- time_gap: array
    |    |-- element: struct
    |    |    |-- property: string
    |    |    |-- time: string 
    |    |    |-- year: integer
    |    |    |-- before_common_era: boolean
    
    Note that the timestamp stores date in Gregorian or Julian calendar, 
    but we ignore the difference. (stored in 'calendarmodel' fields in 
    the data type)
    """
    time_item = (wikidata_properties
        .filter(F.col('property').isin(time_properties_list))
        .withColumn('time', F.json_tuple('value', 'time'))
        .withColumn('year', get_year_udf(F.col('time')))
        .withColumn('before_common_era', F.when(F.substring('time', 1, 1) == '-', True).otherwise(False))
        .withColumn('time_gap', F.struct(F.col('property'), F.col('time'), F.col('year'), F.col('before_common_era')))
        .select('qitem_id', 'time_gap')
        .groupby('qitem_id')
        .agg(F.collect_set('time_gap').alias('time_gap')))
    df = (qitems.alias('q')
        .join(time_item.alias('t'), F.col('q.qitem_id') == F.col('t.qitem_id'), 'left')
        .select('q.*', 't.time_gap'))
    return df


def get_one_dataframe(wikipedia_pages, qitems):
    """Returns a new DataFrame that `wikipedia_pages` DataFrame joins 
    with `qitems` DataFrame on `qitem_id` using left join.
    """
    df = (wikipedia_pages.alias('wp')
        .join(qitems.alias('q'), F.col('wp.qitem_id') == F.col('q.qitem_id'), 'left')
        .select('wp.wiki_db', 'wp.page_id', 'wp.qitem_id', 'wp.page_title', 'wp.create_timestamp', 
                'q.is_human', 'q.gender_gap', 'q.sexual_orientation_gap', 'q.geography_gap', 'q.time_gap'))
    df = df.fillna(False, ['is_human'])
    return df