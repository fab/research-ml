import mwparserfromhell
from pyspark.sql.dataframe import DataFrame
from pyspark.sql import Row, SparkSession, Window
import pyspark.sql.functions as F
import pyspark.sql.types as T
import logging

def extract_wikilinks(wikicode):
    """Extract list of links from wikitext for an article via mwparserfromhell.
    
    This data is much more easily extracted via the pagelinks table but this wikitext-based approach
    is important if you're trying to filter out links added via templates or need context about where
    links are found on a page.
    
    NOTE: this approache removes the section anchors from links so it can be more easily matched against
    other data about page titles but you could remove the `.partition('#')[0]` part to retain them.
    """
    try:
        return [str(l.title).partition('#')[0].replace(' ', '_').strip() for l in wikicode.filter_wikilinks()]
    except Exception:
        logging.exception('error occurred while extracting wikilinks')
        return None

def extract_number_references(wikicode):
    """Extract a count of references from wikitext for an article via mwparserfromhell.
    
    NOTE: the shortened footnote templates are used in some wikis but <ref> tags are definitely the standard
    and it would be fair to just remove that portion of the code.
    """
    sfn_templates = ["Shortened footnote template", "sfn", "Sfnp", "Sfnm", "Sfnmp"]
    try:
        num_ref_tags = len([t.tag for t in wikicode.filter_tags() if t.tag == 'ref'])
        num_sftn_templates = len([t.name for t in wikicode.filter_templates() if t.name in sfn_templates])
        return num_ref_tags + num_sftn_templates
    except Exception:
        logging.exception('error occurred while extracting number of references')
        return None

def extract_headings(wikicode, level=None):
    """Extract list of headings from wikitext for an article via mwparserfromhell."""
    try:
        if level is None:
            return [str(l.title).strip() for l in wikicode.filter_headings()]
        else:
            return [str(l.title).strip() for l in wikicode.filter_headings() if l.level == level]
    except Exception:
        logging.exception('error occurred while extracting headings')
        return None

def extract_templates(wikicode):
    """Extract list of templates from wikitext for an article via mwparserfromhell."""
    try:
        return [str(t.name).replace(' ', '_').strip() for t in wikicode.filter_templates()]
    except Exception:
        logging.exception('error occurred while extracting templates')
        return None

@F.udf(returnType="struct<links:array<string>,num_refs:int,headings:array<string>,templates:array<string>>")
def extract_wikitext_attributes(wikitext):
    """
    The python functions at the top operate on the wikicode allows us to write an udf that parses the wikitext only once.
    """
    features = {}
    wikicode = mwparserfromhell.parse(wikitext)
    features['links'] = extract_wikilinks(wikicode)
    features['num_refs'] = extract_number_references(wikicode)
    features['headings'] = extract_headings(wikicode)
    features['templates'] = extract_templates(wikicode)
    return Row(**features)
