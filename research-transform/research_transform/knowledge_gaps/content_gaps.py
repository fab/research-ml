import pyspark.sql.functions as F
import pyspark.sql.types as T
# %%
def get_wikipedia_projects(spark, mediawiki_snapshot):
    """
    get all languages Wikipedia projects.
    root
    |-- wiki_db: string (nullable = true)
    """
    query = f"""SELECT DISTINCT dbname AS wiki_db
                FROM wmf_raw.mediawiki_project_namespace_map
                WHERE snapshot = '{mediawiki_snapshot}'
                    AND hostname LIKE '%wikipedia%'
            """
    return spark.sql(query)

def wikipedia_pages_df(spark, mediawiki_snapshot, wikidata_snapshot, projects=None):
    """
    retrieve all the wikipedia pages for given projects. default is all wiki projects.
    root
    |-- page_id: long (nullable = true)
    |-- page_title: string (nullable = true)
    |-- wiki_db: string (nullable = true)
    |-- create_timestamp: string (nullable = true)
    |-- qitem_id: string (nullable = true)
    """
    query = f"""
        SELECT p.page_id, p.page_title, p.wiki_db, 
            mp.page_first_edit_timestamp AS create_timestamp,
            wipl.item_id AS qitem_id
        FROM wmf_raw.mediawiki_page p
        INNER JOIN wmf.mediawiki_page_history mp
        ON p.page_id = mp.page_id
            AND p.wiki_db = mp.wiki_db
        LEFT JOIN wmf.wikidata_item_page_link wipl
        ON p.wiki_db = wipl.wiki_db
            AND p.page_id = wipl.page_id
        WHERE p.snapshot='{mediawiki_snapshot}'
            AND p.page_namespace=0
            AND p.page_is_redirect=0
            AND mp.snapshot='{mediawiki_snapshot}'
            AND wipl.snapshot = '{wikidata_snapshot}'
            AND wipl.page_namespace = 0
        """
    df = spark.sql(query).dropDuplicates()
    if projects:
        df = df.where(F.col('wiki_db').isin(projects))
    return df
# %%
def wikidata_qitems_df(spark, wikidata_snapshot):
    """
    retrieve all the wikidata qitems from wikidata_entity table.
    root
    |-- qitem_id: string (nullable = true)
    """
    query = f"""
            SELECT id AS qitem_id
            FROM wmf.wikidata_entity
            WHERE typ = 'item'
                AND snapshot = '{wikidata_snapshot}'
        """ 
    return spark.sql(query)

def wikidata_properties(spark, wikidata_snapshot):
    """
    retrieves all wikidata triples that have Q(item), P(property), Q(item) relation by exploding claims in wikidata_entity table.
    root
    |-- qitem_id: string (nullable = true) - subject
    |-- property: string (nullable = true) - predicate
    |-- value: string (nullable = true) - object

    Wikidata is stored in the form of triples. A triple can be read like a sentence with a subject, a predicate, and an object.
    for example: Abuja (Q3787), capital of (P1376), Nigeria(Q1033): Abuja is the capital city of Nigeria.

    schema of claims: (https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake/Content/Wikidata_entity)
        array<
            struct<
                id: string, 
                mainSnak: struct<typ:string, property:string, dataType:string, dataValue: struct<typ:string,value:string>, hash:string>, 
                typ: string,
                rank: string,
                qualifiers: array<struct<typ:string, property:string, dataType:string, dataValue:struct<typ:string,value:string>, hash:string>>,
                references: array<struct<snaks: array<struct<typ:string, property:string, dataType:string, dataValue:struct<typ:string,value:string>, hash:string>>, order: array<string>, hash:string>>
            >
        >
    """
    query = f"""
            SELECT id AS qitem_id, claim.mainSnak.property, claim.mainsnak.datavalue.value
            FROM wmf.wikidata_entity
            LATERAL VIEW explode(claims) AS claim
            WHERE typ = 'item'
                AND snapshot = '{wikidata_snapshot}'
        """
    return spark.sql(query)

def append_is_human(qitems, wikidata_properties):
    """
    appends a `is_human` column to indicate whether a qitem is human or non-human.
    if a qitem has instance_of(P31) property and contains human(Q5) value, it is True;
        otherwise, it is False
    """
    wikidata_property = (wikidata_properties
        .filter(F.col('property') == 'P31')
        .withColumn('value', F.json_tuple('value', 'id'))
        .groupby('qitem_id')
        .agg(F.collect_list('value').alias('instance_of'))
        .withColumn('is_human', F.array_contains(F.col('instance_of'), 'Q5')) # Q5: human
        .select('qitem_id', 'is_human'))
    df = (qitems.alias('q')
        .join(wikidata_property.alias('p'), F.col('q.qitem_id') == F.col('p.qitem_id'), 'left')
        .select('q.*', 'p.is_human'))
    df = df.na.fill(False, ['is_human'])
    return df

def append_gender(qitems, wikidata_properties):
    """
    appends a `gender` column, string type.
    if a qitem has sex or gender(P21) porperty, it is the gender qid it belongs to;
        otherwise, it is null.
    gender qid such as male (Q6581097), female (Q6581072), non-binary (Q48270), etc.
    """
    wikidata_property = (wikidata_properties
        .filter(F.col('property') == 'P21')
        .withColumn('value', F.json_tuple('value', 'id'))
        .groupby('qitem_id') # there are ~1700 qitems have multiple gender values, 
                             # take first value as gender value
        .agg(F.first('value').alias('gender'))) 
    df = (qitems.alias('q')
        .join(wikidata_property.alias('p'), F.col('q.qitem_id') == F.col('p.qitem_id'), 'left')
        .select('q.*', 'p.gender'))
    return df    

def append_sexual_orientation(qitems, wikidata_properties):
    """
    appends a `sexual_orientation` column, string type.
    if a qitem has sexual orientation(P91) porperty, it is the sexual orientation qid it belongs to;
        otherwise, it is null.
    sexual orientation qid such as heterosexuality (Q1035954), homosexuality (Q6636), bisexuality (Q43200), etc.
    """
    wikidata_property = (wikidata_properties
        .filter(F.col('property') == 'P91')
        .withColumn('value', F.json_tuple('value', 'id'))
        .groupby('qitem_id')  # there are 88 qitems have multiple sexual_orientation values, 
                              # take first value as sexual_orientation value
        .agg(F.first('value').alias('sexual_orientation')))
    df = (qitems.alias('q')
        .join(wikidata_property.alias('p'), F.col('q.qitem_id') == F.col('p.qitem_id'), 'left')
        .select('q.*', 'p.sexual_orientation'))
    return df

def append_partners(qitems, wikidata_properties):
    """
    appends a `partners` column, array type.
    if a qitem has spouse(P26) or unmarried partner(P451) porperty, collects the values into a list;
        otherwise, it is null.
    """
    wikidata_property = (wikidata_properties
        .filter(F.col('property').isin(['P26', 'P451'])) # spouse and partner
        .withColumn('value', F.json_tuple('value', 'id'))
        .groupby('qitem_id')
        .agg(F.collect_list('value').alias('partners')))
    df = (qitems.alias('q')
        .join(wikidata_property.alias('p'), F.col('q.qitem_id') == F.col('p.qitem_id'), 'left')
        .select('q.*', 'p.partners'))    
    return df

def append_geocoordinates(qitems, wikidata_properties):
    """
    appends a `geocoordinates` column, struct<lat:string,lon:string>.
    if a qitem has coordinate location (P625) porperty, forms the lat/lon into a struct;
        otherwise, it is null.
    """
    wikidata_property = (wikidata_properties
        .filter(F.col('property') == 'P625')
        .select(F.col('qitem_id'), F.json_tuple(F.col('value'), 'lat', 'lon'))
        .withColumnRenamed('c0', 'lat')
        .withColumnRenamed('c1', 'lon')
        .withColumn('geocoordinates', F.struct(F.col('lat'), F.col('lon')))
        .drop('lat', 'lon'))
    df = (qitems.alias('q')
        .join(wikidata_property.alias('p'), F.col('q.qitem_id') == F.col('p.qitem_id'), 'left')
        .select('q.*', 'p.geocoordinates'))
    return df
# %%
def wikitext_df(spark, mediawiki_snapshot, projects=None):
    """
    retrieve wikitext for all the wikipedia pages for given projects. default is all wiki projects.
    root
     |-- wiki_db: string (nullable = true)
     |-- page_id: long (nullable = true)
     |-- page_title: string (nullable = true)
     |-- revision_id: long (nullable = true)
     |-- revision_text: string (nullable = true)
    """
    query = f"""
        SELECT mw.wiki_db, mw.page_id, mw.page_title, mw.revision_id, mw.revision_text
        FROM wmf.mediawiki_wikitext_current mw
        LEFT JOIN wmf_raw.mediawiki_page p
            ON p.page_id = mw.page_id
            AND p.wiki_db = mw.wiki_db
        WHERE mw.snapshot = '{mediawiki_snapshot}'
        AND mw.page_namespace = 0
        AND p.snapshot = '{mediawiki_snapshot}'
            AND p.page_namespace = 0
            AND p.page_is_redirect = 0
        """
    df = spark.sql(query)
    if projects:
        df = df.where(F.col('wiki_db').isin(projects))
    return df
