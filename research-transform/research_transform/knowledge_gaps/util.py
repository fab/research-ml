import os
import requests
import csv
import json
from shapely.geometry import shape, Point

def download_supporting_files(cfg):
    """Download supporting files

    1. `region_geoms`, `country_properties_tsv`, `country_aggregation_tsv`, and `region_qids_tsv`
    from geohci/wiki-region-groundtruth (https://github.com/geohci/wiki-region-groundtruth/tree/main/resources)

    2. `country_continent_json`: mapping from country code to continent and sub-continent
    from lukes/ISO-3166-Countries-with-Regional-Codes (https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes)

    3. `gender_categories_tsv`, `sexual_orientation_categories_tsv`, and `time_properties_tsv`
    from https://github.com/AikoChou/wikimedia-research-2021/tree/main/knowledge-gap-metrics (temporary)
    """
    if not os.path.exists(cfg.data_dir):
        os.makedirs(cfg.data_dir)

    with open(os.path.join(cfg.data_dir, cfg.region_geoms[0]), 'wb') as out_file:
        content = requests.get(cfg.region_geoms[1], stream=True).content
        out_file.write(content)

    with open(os.path.join(cfg.data_dir, cfg.country_properties_tsv[0]), 'wb') as out_file:
        content = requests.get(cfg.country_properties_tsv[1], stream=True).content
        out_file.write(content)

    with open(os.path.join(cfg.data_dir, cfg.country_aggregation_tsv[0]), 'wb') as out_file:
        content = requests.get(cfg.country_aggregation_tsv[1], stream=True).content
        out_file.write(content)

    with open(os.path.join(cfg.data_dir, cfg.region_qids_tsv[0]), 'wb') as out_file:
        content = requests.get(cfg.region_qids_tsv[1], stream=True).content
        out_file.write(content)

    with open(os.path.join(cfg.data_dir, cfg.gender_categories_tsv[0]), 'wb') as out_file:
        content = requests.get(cfg.gender_categories_tsv[1], stream=True).content
        out_file.write(content)

    with open(os.path.join(cfg.data_dir, cfg.sexual_orientation_categories_tsv[0]), 'wb') as out_file:
        content = requests.get(cfg.sexual_orientation_categories_tsv[1], stream=True).content
        out_file.write(content)

    with open(os.path.join(cfg.data_dir, cfg.time_properties_tsv[0]), 'wb') as out_file:
        content = requests.get(cfg.time_properties_tsv[1], stream=True).content
        out_file.write(content)

    with open(os.path.join(cfg.data_dir, cfg.country_continent_json[0]), 'wb') as out_file:
        content = requests.get(cfg.country_continent_json[1], stream=True).content
        out_file.write(content)


def get_properties(properties_tsv):
    """Load properties data

    1. List of properties used for linking Wikidata items to regions.
        e.g., P19: place of birth 
    2. List of properties used for linking Wikidata items to datetimes.
        e.g., P569: date of birth
    """
    expected_header = ['Property', 'Label']
    properties = []
    with open(properties_tsv, 'r') as fin:
        tsvreader = csv.reader(fin, delimiter='\t')
        assert next(tsvreader) == expected_header
        for line in tsvreader:
            property = line[0]
            label = line[1]
            properties.append((property, label))
    return properties


def get_categories(categories_tsv):
    """Load categories data QID -> label for labeling

    1. Gender gap categories: 
        allowed values from https://www.wikidata.org/wiki/Property_talk:P21
    2. Sexual Orientation gap categories:
        allowed values from https://www.wikidata.org/wiki/Property_talk:P91
    """
    expected_header = ['Label', 'QID']
    categories = []
    with open(categories_tsv, 'r') as fin:
        tsvreader = csv.reader(fin, delimiter='\t')
        assert next(tsvreader) == expected_header
        for line in tsvreader:
            label = line[0]
            qid = line[1]
            categories.append((qid, label))
    return categories


def get_country_continent(country_continent_json):
    """Load JSON file that maps country code to continent and sub-continent

    * Kosovo is missing in original data. We add it manually. 
    """
    with open(country_continent_json) as fin:
        all_fields = json.load(fin)
    country_continent = []
    for i in range(len(all_fields)):
        name = all_fields[i]['name']
        country_code = all_fields[i]['alpha-2']
        continent = all_fields[i]['region']
        sub_continent = all_fields[i]['sub-region']
        country_continent.append((name, country_code, continent, sub_continent))    
    # add Kosovo
    if 'Kosovo' not in [c[0] for c in country_continent]:
        country_continent.append(('Kosovo', 'XK', 'Europe', 'Southern Europe'))
    return country_continent


def get_aggregation_logic(aggregates_tsv):
    """Mapping of QIDs -> regions not directly associated with them.

    e.g., Sahrawi Arab Democratic Republic (Q40362) -> Western Sahara (Q6250)
    """
    expected_header = ['Aggregation', 'From', 'QID To', 'QID From']
    aggregation = {}
    with open(aggregates_tsv, 'r') as fin:
        tsvreader = csv.reader(fin, delimiter='\t')
        assert next(tsvreader) == expected_header
        for line in tsvreader:
            try:
                qid_to = line[2]
                qid_from = line[3]
            except Exception:
                print("Skipped:", line)
            if qid_from:
                aggregation[qid_from] = qid_to
    return aggregation


def get_qid_to_region(region_qids_tsv, aggregation_tsv):
    """Load QID -> region name for labeling

    1. base regions QIDs
    2. additional QIDs for aggregation
    """
    # load in canonical mapping of QID -> region name for labeling
    qid_to_region = {}
    with open(region_qids_tsv, 'r') as fin:
        tsvreader = csv.reader(fin, delimiter='\t')
        assert next(tsvreader) == ['Region', 'QID']
        for line in tsvreader:
            region = line[0]
            qid = line[1]
            qid_to_region[qid] = region
    print("\nLoaded {0} QID-region pairs for matching against Wikidata -- e.g., Q31: {1}".format(
        len(qid_to_region), qid_to_region['Q31']))
    # load in additional QIDs that should be mapped to a more canonical region name
    aggregation = get_aggregation_logic(aggregation_tsv)
    for qid_from in aggregation:
        qid_to = aggregation[qid_from]
        if qid_to in qid_to_region:
            qid_to_region[qid_from] = qid_to_region[qid_to]  
        else:
            print("-- Skipping aggregation for {0} to {1}".format(qid_from, qid_to))
    print("Now {0} QID-region pairs after adding aggregations -- e.g., Q40362: {1}".format(
        len(qid_to_region), qid_to_region['Q40362']))    
    return qid_to_region


def get_region_data(qid_to_region, region_geoms_geojson):
    """Load geometries used for the regions identified via Wikidata
    """
    # load in geometries for the regions identified via Wikidata
    with open(region_geoms_geojson, 'r') as fin:
        regions = json.load(fin)['features']
    region_shapes = {}
    skipped = []
    for c in regions:
        qid = c['properties']['WIKIDATAID']
        if qid in qid_to_region:
            region_shapes[qid] = shape(c['geometry'])
        else:
            skipped.append('{0} ({1})'.format(c['properties']['NAME'], qid))
    print("\nLoaded {0} region geometries. Skipped {1}: {2}".format(
        len(region_shapes), len(skipped), skipped))
    
    # check alignment between QID list and region geometries
    in_common = 0
    for qid in qid_to_region:
        if qid in region_shapes:
            in_common += 1
        else:
            alt_found = False
            for qid_alt in qid_to_region:
                if qid != qid_alt and qid_to_region[qid] == qid_to_region[qid_alt]:
                    alt_found = True
            if not alt_found:
                print('Prop-only: {0} ({1})'.format(qid_to_region[qid], qid))
    print("{0} QIDs in common between prop-values and geometries.".format(in_common))
    return region_shapes
