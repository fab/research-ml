import base64
from datetime import timedelta
import hashlib
from io import BytesIO
from pyspark.sql import Row
import pyspark.sql.functions as F
import pyspark.sql.types as T
import pyspark
import requests

from PIL import Image
# 400px
mb_per_image_400px = 61.8e3/1e6
# 300px
mb_per_image_300px = 39.4e3/1e6
# measured with 400px
# images_per_second_per_thread = 1e6/(4.6*60*60)/(20*4)
mb_per_second_per_thread = 61.8e3/(4.6*60*60)/(20*4)
# images on commons to download
num_images_commons = 53e6

def estimate_partitions(num_files, mb_per_file, file_size_mb):
    num_partitions = int(num_files*mb_per_file/file_size_mb)
    total_mb = num_files*mb_per_file
    total_seconds = total_mb/mb_per_second_per_thread/threads_querying_swift
    print(f"""
    estimate required number of partitions
    {num_files} files {mb_per_file}mb each -> expected size {num_files*mb_per_file/1e3}GB
    use {num_partitions} partitions for files of size {file_size_mb}mb
    expected total duration with {threads_querying_swift} cores: {str(timedelta(seconds=int(total_seconds)))}
    """)
    return max(num_partitions,1)

# TODO these methods should be in their own module
def decode_b64(image_b64):
    """decode the base64 encoded image bytes into a byte array"""
    return base64.b64decode(image_b64)

def open_image(image_bytes):
    """returns a PIL.Image from bytes"""
    return Image.open(BytesIO(image_bytes))

image_schema="""
struct<
    image_bytes_b64:string,
    format:string,
    width:int,
    height:int,
    image_bytes_sha1:string,
    error:string
>
"""

@F.udf(returnType=image_schema)
def parse_image(image_bytes_b64):
    try:
        image_bytes = decode_b64(image_bytes_b64)
        image = open_image(image_bytes)
        image_bytes_sha1 = hashlib.sha1(image_bytes).hexdigest()
        return Row(
            image_bytes_b64=image_bytes_b64,
            format=image.format,
            width=image.width,
            height=image.height,
            image_bytes_sha1=image_bytes_sha1,
            error=None
        )
    except Exception as e:
        print(f"Error parsing image bytes. Exception: {e}")
        return Row(
            image_bytes_b64=image_bytes_b64,
            format=None,
            width=None,
            height=None,
            image_bytes_sha1=None,
            error=str(e))


swift_host = "https://ms-fe.svc.eqiad.wmnet"
max_retries = 5
timeout = 15

def get_file_bytes(project, file_name, thumb_size):
    md5 = hashlib.md5(file_name.encode('utf-8')).hexdigest()
    shard = f"{md5[0]}/{md5[:2]}"
    if thumb_size:
        uri = f"{swift_host}/wikipedia/{project}/thumb/{shard}/{file_name}/{thumb_size}-{file_name}"
    else:
        uri = f"{swift_host}/wikipedia/{project}/{shard}/{file_name}"

    # res = requests.get(uri, verify=True, timeout=timeout)
    res = requests.get(uri, stream=True, verify=True, timeout=timeout)
    res.raise_for_status()
    max_file_size=10e6 # ~10mb
    res_bytes = b''
    size = 0
    for chunk in res.iter_content(1024):
        res_bytes = res_bytes + chunk
        size += len(chunk)
        if size > max_file_size:
            raise ValueError(f'file larger than {max_file_size} bytes')
    return res_bytes

@F.udf(returnType=T.ArrayType(T.StringType()))
def download_file(file_name, project, thumb_size):
    #requests's built in retries causes some dependency issue on the worker, doing retries manually
    error=None
    for retry_count in range(max_retries):
        try:
            file_bytes = get_file_bytes(project, file_name, thumb_size)
            return (base64.b64encode(file_bytes).decode('utf-8'), None)
        except requests.exceptions.HTTPError as e:
            print(f"try {retry_count}, swift download error for file {file_name}. Exception: {e}")
            error=str(e)
            if e.response.status_code==404:
                # no need to retry
                break
        except ValueError as e:
            print(f"file {file_name} : {str(e)}")
            error=str(e)
            break
        except Exception as e:
            print(f"try {retry_count}, swift download error for file {file_name}. Exception: {e}")
            error=str(e)
    return (None, error)

def download_files(input_df, partitions, thumb_size=None):
    """
    `input_df` is expected to have a `file_name` and a `project` column.
    `thumb_size` int used for download image file types

    returns two dataframes
        `input` dataframe with a `file_bytes_b64` column containing the raw file bytes as base64 encoded string
        `input` dataframe with a `download_error` column containing the errors for each file that wasn't downloaded
    """

    input_df = input_df.repartition(partitions) if partitions is not None else input_df

    thumb_col = F.lit(f"{thumb_size}px") if thumb_size else F.lit(None)
    input_df = input_df.withColumn('thumbnail_size', thumb_col)

    attempted_file_bytes = (input_df
        .withColumn('attempted_files_bytes', download_file('file_name', 'project', 'thumbnail_size'))
        .drop('thumbnail_size')
        .persist(pyspark.StorageLevel.DISK_ONLY))

    download_errors = (attempted_file_bytes
        .withColumn('download_error', F.col('attempted_files_bytes').getItem(1))
        .where(F.col('download_error').isNotNull())
        .drop('attempted_files_bytes'))

    with_file_bytes = (attempted_file_bytes
        .where(F.col('attempted_files_bytes').getItem(0).isNotNull())
        .withColumn('file_bytes_b64', F.col('attempted_files_bytes').getItem(0))
        # .withColumn('image', parse_image(F.col('attempted_files_bytes').getItem(0)))
        .drop('attempted_files_bytes'))

    return (with_file_bytes, download_errors)

def append_image_bytes(
    input_df,
    mb_per_file=None,
    output_dir=None,
    partition_size_mb=200,
    thumb_size=None,
    swift_download_errors_dir=None):
    """
    `input_df` DataFrame is required to have columns `file_name` and `project`.
    `mb_per_file` if passed, the file size is used to estimate the number of
        partitions required to generate partitions of size `partition_size_mb`
    `output_dir` if passed the directory where avro files are written to
    `thumb_size` int, can be used for download image file types
    `swift_download_errors_dir` if passed, the swift download errors are stored on disk

    The files from in the input dataframe are retrieved from swift, then the input
    columns and the new file column `file_bytes_b64` are written to avro encoded files in the
    `output_dir` directory.
    """
    num_partitions=None
    if mb_per_file:
        print('counting total number of files for file partitioning')
        num_files = input_df.count()
        num_partitions = estimate_partitions(num_files=num_files,mb_per_file=mb_per_file,file_size_mb=partition_size_mb)

    files, errors = download_files(
        input_df=input_df,
        partitions=num_partitions,
        thumb_size=thumb_size)

    if output_dir:
        print(f"downloading files into {files.rdd.getNumPartitions()} partitions and writing them to {output_dir}")
        files.write.format("avro").mode("overwrite").save(output_dir)

    if swift_download_errors_dir:
        print(f"writing errors from swift to {swift_download_errors_dir}")
        errors.write.format("avro").mode("overwrite").save(swift_download_errors_dir)

    return files, errors
