import pyspark.sql.functions as F
import pyspark.sql.types as T
from research_transform.swift import swift


def download_commons_media(
    spark,
    snapshot, # '2021-08',
    media_output_dir, # ='test',
    wiki_dbs=None, # ['commons'],
    prefixes=['File:'],
    suffixes=None, # ['.wav'] or ['.png', '.jpg', '.jpeg', '.gif'],
    mb_per_file=None, # 0.3
    thumb_size=None, # 400
    partition_size_mb=200,
    swift_download_errors_dir=None):
    """"
    Advisory: the spark context will be used to query the swift cluster
    """
    # todo special case: no spark allowed to be passed in?
    # spark.stop()
    # spark = swift.create_spark_context_for_swift()

    history = spark.sql(f"""
    select * from wmf.mediawiki_wikitext_current where snapshot='{snapshot}'
    """)

    if wiki_dbs:
        wikis = [f'{db}wiki' for db in wiki_dbs]
        history = history.where(F.col('wiki_db').isin(wikis))


    @F.udf(returnType=T.BooleanType())
    def filter_files(page_title):
        """Return true if we are generally interested in this row"""
        good_start = not prefixes or any(page_title.startswith(prefix) for prefix in prefixes)
        good_end = not suffixes or any(page_title.endswith(suffix) for suffix in suffixes)
        return good_start and good_end

    page_to_filename = F.udf(lambda pt: pt[5:].replace(" ", "_"), 'string')

    history = (history
        .filter(filter_files('page_title'))
        .withColumn("file_name", page_to_filename('page_title'))
        # todo
        .withColumn("project", F.lit('commons'))
        )

    swift.append_image_bytes(
        input_df=history,
        mb_per_file=mb_per_file,
        output_dir=media_output_dir,
        thumb_size=thumb_size,
        partition_size_mb=partition_size_mb,
        swift_download_errors_dir=swift_download_errors_dir)




